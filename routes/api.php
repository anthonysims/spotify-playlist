<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/spotify/search','SpotifyController@search')->name('spotify.search');
Route::post('spotify/add','SpotifyController@addTrack')->name('spotify.add');

Route::middleware('auth:web')->post('playlist/set','HomeController@setPlaylist')->name('playlist.set');
