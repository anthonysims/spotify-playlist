<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('search');
})->middleware('check-online');

Route::get('offline', function () {
    return view('offline');
});

Route::middleware(['auth'])->group(function() {
    Route::get('spotify/auth','SpotifyController@auth');
    Route::get('spotify/callback','SpotifyController@callback');
    Route::get('admin/playlists','HomeController@playlists')->name('playlists');
    Route::post('admin/playlist','HomeController@setPlaylist')->name('setPlaylist');
    Route::get('admin/settings','SettingsController@index');
    Route::put('admin/settings','SettingsController@update');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
