<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpotifyApi;
use App\Playlist;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function playlists()
    {
        $spotify = new SpotifyApi();
        $playlists = $spotify->getMyPlaylists();
        $setPlaylist = Playlist::find(1);

        return view('playlists',['playlists' => $playlists, 'setPlaylist' => $setPlaylist->playlist_id]);
    }

    /**
     * Set the configured playlist
     * 
     * @param string $playlistId
     */
    public function setPlaylist(Request $request) {
        Playlist::updateOrCreate(['id' => 1],
        [
            'playlist_id' => $request->id,
        ]);

        $response = [
            'status' => 'Success'
        ];

        return response()->json($response);
    }
}
