<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Option;

class SettingsController extends Controller
{
    public function index() {
        $options = (new Option())->getAll();

        return view('admin.settings.index',['options' => $options]);
    }

    public function update(Request $request) {
        $options = $request->except(['_token','_method']);
        
        foreach ($options as $name => $value) {
            Option::updateOrCreate([
                'name' => $name,
            ],
            [
                'name' => $name,
                'value' => $value
            ]);
        }

        return redirect('admin/settings')->with('status','Settings Saved Successfully');
    }
}
