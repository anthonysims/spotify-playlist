<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Spotify;
use App\SpotifyApi;
use App\Playlist;

class SpotifyController extends Controller
{
    public function auth() {
        $spotify = new \SpotifyWebAPI\Session(
            config('spotify.client_id'),
            config('spotify.client_secret'),
            config('spotify.redirect_uri')
        );

        $options = [
            'scope' => [
                'playlist-read-private',
                'playlist-modify-private'
            ]
        ];

        return redirect($spotify->getAuthorizeUrl($options));
    }
    public function callback() {
        $spotify = new \SpotifyWebAPI\Session(
            config('spotify.client_id'),
            config('spotify.client_secret'),
            config('spotify.redirect_uri')
        );

        $spotify->requestAccessToken($_GET['code']);

        $accessToken = $spotify->getAccessToken();
        $refreshToken = $spotify->getRefreshToken();
        $expiresAt = $spotify->getTokenExpiration();

        $token = Spotify::updateOrCreate(
            ['id' => 1],
            [
                'access_token' => $accessToken,
                'refresh_token' => $refreshToken,
                'expires_at' => Carbon::createFromTimestamp($expiresAt)
            ]
        );

        return redirect()->route('home');
    }

    public function search(Request $request) {
        $api = new SpotifyApi();

        $q = $request->search;

        $results = $api->search($q,['track']);

        return response()->json($results);
    }

    public function addTrack(Request $request) {
        $api = new SpotifyApi();
        $playlist = Playlist::find(1);

        $reponse = $api->addPlaylistTracks($playlist->playlist_id,$request->trackId);

        return response()->json(['success' => $reponse]);
    }
}
