<?php

namespace App\Http\Middleware;

use Closure;
use App\Option;

class CheckOnline
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $online = Option::where('name','online')->first();

        if (is_null($online) || $online->value != 'on') {
            return response()->view('offline');
        }
        return $next($request);
    }
}
