<?php

namespace App;
use App\Spotify;
use Illuminate\Support\Carbon;
use \SpotifyWebAPI;

class SpotifyApi {

    protected $api;

    public function __construct() {
        $spotify = Spotify::first();
        $expiryDate = new Carbon($spotify->expires_at);
        $api = new SpotifyWebAPI\SpotifyWebAPI();
        $accessToken = $spotify->access_token;

        if ($expiryDate->isPast()) {
            $accessToken = $this->refreshToken();
        }

        $api->setAccessToken($accessToken);

        $this->api = $api;
    }

    public function __call($name, $arguments) {
        $api = $this->api;
        return call_user_func_array([$api,$name],$arguments);
    }

    public function createSession() {
        $session = new SpotifyWebAPI\Session(
            config('spotify.client_id'),
            config('spotify.client_secret'),
            config('spotify.redirect_uri')
        );

        return $session;
    }
    
    public function refreshToken() {
        $spotify = Spotify::first();
        $refreshToken = $spotify->refresh_token;

        $session = $this->createSession();
        $session->refreshAccessToken($refreshToken);

        $accessToken = $session->getAccessToken();
        $expiresAt = $session->getTokenExpiration();

        $spotify->access_token = $accessToken;
        $spotify->expires_at = Carbon::createFromTimestamp($expiresAt);
        $spotify->save();

        return $accessToken;
    }
}