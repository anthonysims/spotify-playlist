<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded = ['id'];

    public function getAll() {
        $raw = $this->all();
        $options = [];

        foreach ($raw as $option) {
            $options[$option->name] = $option->value;
        }

        return $options;
    }
}
