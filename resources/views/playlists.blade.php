@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Playlists</div>
                <playlists :playlists='@json($playlists->items)' :pre-set='"{{$setPlaylist}}"'></playlists>
            </div>
        </div>
    </div>
</div>
@endsection
