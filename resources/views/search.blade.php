@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <search-form></search-form>
            </div>
        </div>
        <search-results></search-results>
    </div>
@endsection
