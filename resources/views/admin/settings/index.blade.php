@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-5">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <h1>Settings</h1>
            <form action="/admin/settings" method="post">
                @csrf
                @method('PUT')

                <div class="form-check">
                    <input type="hidden" name="online" value="off">
                    <input type="checkbox" class="form-check-input" 
                    @if ($options['online'] == 'on')
                        checked
                    @endif
                     name="online" id="online">
                    <label for="online">Site Online</label>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</div>

@endsection